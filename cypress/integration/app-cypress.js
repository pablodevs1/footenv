/// <reference types="Cypress" />

describe('FootEnv APP', () => {
  beforeEach(() => {
    cy.visit('/');
    cy.intercept('POST', 'https://beta3.api.climatiq.io/batch', { fixture: './../../fixtures/batch.json' }).as('batchFetch');
  });

  it('home page can be opened', () => {
    cy.contains('Earth Loves You,');
    cy.contains('Love it Back');
  });

  it('questionnaire page can be visited', () => {
    cy.contains('Take questionnaire').click();
    cy.get('#questionnaireTitle').should('contain.text', 'Questionnaire');
    cy.get('#question_20-1').should('not.exist');
  });

  it('user can answer all the questions', () => {
    cy.contains('Take questionnaire').click();
    cy.contains('Did you know?');
    cy.get('#question_1-3').click();
    cy.get('#question_2-0').click();
    cy.get('#question_3-1').click();
    cy.get('#question_4-4').click();
  });

  describe('when questionnaire is answered', () => {
    beforeEach(() => {
      cy.visit('/questionnaire');
      cy.answerQuestionnaire();
    });

    it('results show up correctly', () => {
      cy.get('.results-ui__charts > :nth-child(2)').shadow().as('firstChart');

      cy.contains('Results');
      cy.contains('7.82 Tonnes');
      cy.contains('211');

      cy.get('@firstChart')
        .find('.badge')
        .should('contain', '40%');
      cy.get('@firstChart')
        .find('.label')
        .should('contain', 'Clothes');
    });

    it('user can take questionnaire again', () => {
      cy.contains('Retake Questionnaire').click();
      cy.answerQuestionnaire();
      cy.contains('Results');
    });

    it('user can go back to home', () => {
      cy.contains('Go back Home').click();
      cy.contains('Earth Loves You,');
    });
  });
});
