import '@testing-library/cypress/add-commands';

Cypress.Commands.add('answerQuestionnaire', () => {
  cy.get('input[name="question_1"]:first').click();
  cy.get('input[name="question_2"]:last').click();
  cy.get('input[name="question_3"]:first').click();
  cy.get('input[name="question_4"]:last').click();
});
