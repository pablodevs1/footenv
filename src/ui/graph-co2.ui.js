import { html, LitElement } from 'lit';
import { snapshot } from 'valtio/vanilla';
import { state } from '../states/state';
import '../styles/components/graph-co2.css';

export class GraphCo2UI extends LitElement {
  static get properties () {
    return {
      totalCO2: { type: Number }
    };
  }

  constructor () {
    super();

    this.totalCO2 = snapshot(state).totalCO2;
  }

  render () {
    return html`
      <h4>AQUÍ IRÁ EL GRÁFICO COMPARATIVO</h4>
    `;
  }

  createRenderRoot () {
    return this;
  }
}

customElements.define('genk-graph-co2', GraphCo2UI);
