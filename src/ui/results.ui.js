import { html, LitElement } from 'lit';
import { snapshot } from 'valtio/vanilla';
import '../components/chart.component';
import { state } from '../states/state';
import '../styles/components/results-ui.css';

const TREES_PER_TONNE_OF_CO2 = 27;

export class ResultsUI extends LitElement {
  render () {
    return html`
      <header class="results-ui__header">
        <p class="size-400">
          <span>Your Carbon Footprint is:</span>
          </br>
          <span id="footprint" class="co2Footprint">${`${snapshot(state).totalCO2} Tonnes`}</span>
          </br>
          CO2 is one of the most important greenhouse gases. Trees extract CO2 from the air and convert it into oxygen and plant material through photosynthesis. Photosynthesis is the process by which plants convert water and carbon dioxide into glucose (C6H12O6) and oxygen using sunlight.
          </br>
          </br>
          It can be concluded that an average of <span class="treesFootprint">🌲 ${Math.trunc(TREES_PER_TONNE_OF_CO2 * snapshot(state).totalCO2)} 🌲</span> trees are needed to compensate your annual carbon footprint.
        </p>
      </header>
      <div class="results-ui__charts">
        <h2>Contributions to your carbon footprint</h2>
        ${snapshot(state).charts.map((chart) => html`
        <genk-chart .percentage=${chart.percentage} .value=${chart.value} .color="${chart.color}" .category="${chart.category}"></genk-chart>`
    )}
      </div>
      <nav class="results-ui__navigation" aria-label="secondary navigation">
        <a title="Take questionnaire again" href="/questionnaire">Retake Questionnaire</a>
      </nav>
    `;
  }

  createRenderRoot () {
    return this;
  }
}

customElements.define('genk-results-summary', ResultsUI);
