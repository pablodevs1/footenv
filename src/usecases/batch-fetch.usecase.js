import { BatchFetchRepository } from '../repositories/batch-fetch.repository';

export class BatchFetchUseCase {
  async execute (request) {
    const noFetchData = [];
    const requestData = [];

    const repository = new BatchFetchRepository();
    request.forEach((data) => {
      if (data.emission_factor.hasOwnProperty('nofetch')) {
        noFetchData.push({
          ...data.parameters,
          emission_factor: { ...data.emission_factor }
        });
      } else {
        requestData.push(data);
      }
    });

    const response = await repository.batchFetch(requestData);
    return [...response.results, ...noFetchData];
  }
}
