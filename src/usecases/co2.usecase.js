import { UtilsService } from '../services/utils.service';

export class Co2UseCase {
  execute (results) {
    return UtilsService.trunc(results.reduce((acc, currentResult) => acc + currentResult.co2e, 0) / 1000);
  }
}
