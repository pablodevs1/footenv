export class RequestUseCase {
  execute (questions, answerInfo) {
    const [questionInfo, answerIndex] = answerInfo.split('-');
    const questionIndex = questionInfo.split('_')[1] - 1;

    // key-value pairs that will be sent to the API:
    const parameters = questions[questionIndex].answers[answerIndex].parameters;
    const emission_factor = questions[questionIndex].emission_factor;
    return { emission_factor, parameters };
  }
}
