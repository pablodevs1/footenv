import Negative9 from '../assets/negative/negative9.png';
import Positive3 from '../assets/positive/positive3.png';
import Positive4 from '../assets/positive/positive4.png';
import Positive7 from '../assets/positive/positive7.png';

const QUESTIONS = [
  {
    id: 'question_1',
    category: 'Clothes',
    label: 'Clothes',
    emission_factor: {
      id: 'consumer_goods-type_clothing',
      region: 'US'
    },
    title: {
      en: 'How much do you spend on clothes and footwear per month?',
      es: '¿Cuánto gastas en ropa al mes?'
    },
    answers: [
      {
        text: '0€',
        parameters: {
          money: 0,
          money_unit: 'eur'
        }
      },
      {
        text: '5€ - 20€',
        parameters: {
          money: 13 * 12,
          money_unit: 'eur'
        }
      },
      {
        text: '20€ - 50€',
        parameters: {
          money: 35 * 12,
          money_unit: 'eur'
        }
      },
      {
        text: '50€ - 100€',
        parameters: {
          money: 75 * 12,
          money_unit: 'eur'
        }
      },
      {
        text: 'more than 100€',
        parameters: {
          money: 120 * 12,
          money_unit: 'eur'
        }
      }
    ],
    hint: {
      text: 'The clothing industry is a major contributor to pollution and global-warming: the global fashion industry releases an estimated 1.2 billion tonnes of carbon dioxide annually. However, some brands are more sustainable than others (you can look up eco-friendly brands in the link below).',
      link: 'https://en.clear-fashion.com/',
      image: Positive3,
      alt: 'Green companies'
    }
  },
  {
    id: 'question_2',
    category: 'Traveling',
    label: 'Traveling',
    emission_factor: {
      id: 'accommodation_type_hotel_stay',
      region: 'ES'
    },
    title: {
      en: 'How many nights do you spend in a hotel per year?',
      es: '¿Cuántas noches pasas en un hotel al año?'
    },
    answers: [
      {
        text: '0 nights',
        parameters: {
          number: 0
        }
      },
      {
        text: '1 - 3 nights',
        parameters: {
          number: 2
        }
      },
      {
        text: '3 - 7 nights',
        parameters: {
          number: 5
        }
      },
      {
        text: '1 - 2 weeks',
        parameters: {
          number: 11
        }
      },
      {
        text: '2 - 4 weeks',
        parameters: {
          number: 23
        }
      },
      {
        text: 'more than 1 month',
        parameters: {
          number: 40
        }
      }
    ],
    hint: {
      text: 'Hotels can also have a direct negative impact on biodiversity, due to excessive use of resources, purchasing unsustainably farmed produce, waste, irresponsible tourism, and unsustainable design. Their environmental impact can for example be reduced by purchasing environmentally-friendly products and/or purchasing locally.',
      link: 'https://sustainablehospitalityalliance.org/our-work/climate-action/ ',
      image: Positive7,
      alt: 'Green house'
    }
  },
  {
    id: 'question_3',
    category: 'Diet',
    label: 'Diet',
    emission_factor: {
      id: 'diet',
      region: 'UK',
      category: 'Diet',
      nofetch: true
    },
    title: {
      en: 'My diet is mostly:',
      es: 'Mi dieta es sobre todo:'
    },
    answers: [
      {
        text: 'meat lover',
        parameters: {
          co2e: 7.19 * 365,
          co2e_unit: 'kg'
        }
      },
      {
        text: 'omnivore',
        parameters: {
          co2e: 5.63 * 365,
          co2e_unit: 'kg'
        }
      },
      {
        text: 'no beef',
        parameters: {
          co2e: 4.67 * 365,
          co2e_unit: 'kg'
        }
      },
      {
        text: 'fish-eater',
        parameters: {
          co2e: 3.91 * 365,
          co2e_unit: 'kg'
        }
      },
      {
        text: 'vegetarian',
        parameters: {
          co2e: 3.81 * 365,
          co2e_unit: 'kg'
        }
      },
      {
        text: 'vegan',
        parameters: {
          co2e: 2.89 * 365,
          co2e_unit: 'kg'
        }
      }
    ],
    hint: {
      text: 'Meat products have bigger carbon footprints per calorie than vegetables or grains. For example, between 0.5kg and 1.2kg of CO2 is emitted for 1000 kcal of vegetable or grain vs 36kg of CO2 when consuming 1000 kcal of beef.',
      link: 'https://ourworldindata.org/grapher/ghg-kcal-poore',
      image: Negative9,
      alt: 'Meat'
    }
  },
  {
    id: 'question_4',
    category: 'Electricity',
    label: 'Electricity',
    emission_factor: {
      id: 'electricity-energy_source_gas',
      region: 'GB'
    },
    title: {
      en: 'Approximately how much do you pay each month for electricity service?',
      es: '¿Aproximadamente, cuánto pagas al mes por el servicio de energía eléctrica?'
    },
    answers: [
      {
        text: 'less than 40€',
        parameters: {
          money: 40 * 12,
          money_unit: 'eur'
        }
      },
      {
        text: '40€ - 50€',
        parameters: {
          money: 45 * 12,
          money_unit: 'eur'
        }
      },
      {
        text: '50€ - 60€',
        parameters: {
          money: 55 * 12,
          money_unit: 'eur'
        }
      },
      {
        text: '60€ - 70€',
        parameters: {
          money: 65 * 12,
          money_unit: 'eur'
        }
      },
      {
        text: 'more than 70€',
        parameters: {
          money: 70 * 12,
          money_unit: 'eur'
        }
      }
    ],
    hint: {
      text: 'Solar energy can power your entire home: most solar panels are created to match your daily energy consumption needs, including air conditioners and other large appliances. Moreover, the cost of solar panels has dropped by 80% since 2008.',
      link: 'https://energysavingtrust.org.uk/advice/solar-panels/',
      image: Positive4,
      alt: 'Solar panel'
    }
  }
];

export default QUESTIONS;
