export class UtilsService {
  static trunc (value) {
    return Math.round((value + Number.EPSILON) * 100) / 100;
  }
}
