import axios from 'axios';

export class BatchFetchRepository {
  async batchFetch (request) {
    return await (
      await axios({
        method: 'post',
        url: 'https://beta3.api.climatiq.io/batch',
        data: request,
        headers: {
          Authorization: 'Bearer WHGR11YNSJ4AZHGXY4BPRX7JH2FY'
        }
      })
    ).data;
  }
}
