import { Router } from '@vaadin/router';
import './pages/home.page';
import './pages/questionnaire.page';
import './pages/results.page';

const outlet = document.querySelector('#outlet');
const router = new Router(outlet);

router.setRoutes([
  { path: '/', component: 'genk-home-page' },
  { path: '/questionnaire', component: 'genk-questionnaire-page' },
  { path: '/results', component: 'genk-results-page' },
  { path: '(.*)', redirect: '/' }
]);

window.addEventListener('vaadin-router-location-changed', (event) => {
  const page = event.detail.location.pathname.substring(1);
  if (page) {
    document.title = `FootEnv - ${page[0].toUpperCase()}${page.substring(1)}`;
  } else {
    document.title = 'FootEnv';
  }
});
