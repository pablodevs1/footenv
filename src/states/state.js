import { proxy } from 'valtio/vanilla';

export const state = proxy({ totalCO2: null, charts: [] });
