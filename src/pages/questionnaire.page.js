import '../components/fetch.component';
import '../components/questionnaire.component';
import '../components/carousel.component';
import QUESTIONS from '../services/questions.service';

export class QuestionnairePage extends HTMLElement {
  constructor () {
    super();
  }

  connectedCallback () {
    this.innerHTML = `
      <style>
        genk-questionnaire-page {
          display: flex;
          flex-direction: column;
        }

        .questionnaire-header {
          padding: 1rem 0 0 1.5rem;
        }
        .questionnaire-header > nav {
          order: -1;
        }

        .questionnaire-wrapper {
          display: flex;
          flex: 1;
          margin-top: 0.5rem;
        }

        .hints {
          width: 40%;
          min-width: 400px;
          display: flex;
          justify-content: center;
          padding: 2.5rem 2rem 0 2rem;
          border-top-left-radius: 0.5rem;
          background-color: var(--clr-dark-100);
          box-shadow: -6px -2px 20px rgba(0, 0, 0, 0.1);
        }

        .hints__carousel {
          height: 85%;
        }
        .hint__title {
          margin-bottom: 1em;
          color: var(--clr-secondary-300)
        }
        .hint__img {
          width: 180px;
          margin: 1rem auto 2rem auto;
        }

        .hints-checkbox {
          -webkit-appearance: none;
          appearance: none;
          background-color: var(--clr-primary-100);
          margin: 0;
          display: none;
        }
        .hints-toggle {
          display: none;
        }

        @media only screen and (max-width: 999.9px) {
          .questionnaire-wrapper {
            position: relative;
            flex-grow: none;
            overflow-x: hidden;
          }
          .questionnaire-wrapper > genk-fetch {
            width: 100%;
            box-shadow: -6px 0px 20px rgba(0, 0, 0, 0.1);
          }
          .hints {
            min-width: auto;
            width: min(100%, 450px);
            position: absolute;
            top: 0;
            bottom: 0;
            right: 0;
            margin-top: 1rem;
            box-shadow: -6px 5px 18px rgba(0, 0, 0, 0.1);

            transform: translateX(100%);
            transition: transform 0.3s ease;
          }
          .progress-bar {
            right: auto;
          }

          .hints-toggle {
            display: flex;
            align-items: center;
            cursor: pointer;
            z-index: 1;
            position: absolute;
            bottom: 5%;
            right: 0;
            height: 5rem;
            padding-inline: 1.5rem;
            font-size: 0.9em;
            color: var(--clr-primary-400);
            font-weight: 600;

            border-radius: 10rem 0 0 10rem;
            background-color: var(--clr-dark-100);
            box-shadow: -4px -4px 17px 2px rgba(0, 0, 0, 0.15);

            transition: transform 0.3s ease;
          }
          .hints-checkbox:checked ~ .hints-toggle {
            color: var(--clr-dark-100);
            transform: translateX(5rem);
          }
          .hints-checkbox:checked ~ .hints-toggle::before {
            content: '✕';
            color: var(--clr-dark-500);
            font-size: 1.5em;
          }
          .hints-checkbox:checked ~ .hints {
            transform: translateX(0);
          }
        }
      </style>
      <header class="questionnaire-header page-header">
        <h1 id="questionnaireTitle">Questionnaire</h1>
        <nav aria-label="primary navigation" class="primary-nav">
          <a href="/" title="HOME">
            Go back Home
          </a>
        </nav>
      </header>
      <section class="questionnaire-wrapper" aria-label="Questionnaire form">
        <input type="checkbox" id="hints-checkbox" class="hints-checkbox">
        <label for="hints-checkbox" class="hints-toggle" aria-label="toggle hints">
          SHOW HINT
        </label>
        <genk-fetch>
          <genk-questionnaire></genk-questionnaire>
        </genk-fetch>
        <aside id="#hints" class="hints">
          <genk-carousel class="hints__carousel">
          ${QUESTIONS.map((question, index) => {
      return `
            <article id="${`hint-${question.id}-${index}`}" class="hint">
              <img class="hint__img" loading="lazy" src="${question.hint.image}" alt="${question.hint.alt}">
              <header class="hint__header">
                <h2 class="hint__title">Did you know?</h2>
              </header>
              <p class="hint__content">${question.hint.text}</p>
              <footer class="hint__footer">
                <a href="${question.hint.link}" target="_blank" class="learn-more">Learn more</a>
              </footer>
            </article>
            `;
    }).join('')}
    </genk-carousel>
        </aside>
      </section>
      `;
  }
}

customElements.define('genk-questionnaire-page', QuestionnairePage);
