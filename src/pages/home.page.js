import { snapshot } from 'valtio/vanilla';
import BannerImg from '../assets/banner.png';
import Positive1 from '../assets/positive/positive1.png';
import Positive2 from '../assets/positive/positive2.png';
import Positive3 from '../assets/positive/positive3.png';
import Positive4 from '../assets/positive/positive4.png';
import Positive5 from '../assets/positive/positive5.png';
import Positive6 from '../assets/positive/positive6.png';
import Negative1 from '../assets/negative/negative1.png';
import Negative2 from '../assets/negative/negative2.png';
import Negative3 from '../assets/negative/negative3.png';
import Negative4 from '../assets/negative/negative4.png';
import Negative5 from '../assets/negative/negative5.png';
import Negative6 from '../assets/negative/negative6.png';
import Negative7 from '../assets/negative/negative7.png';
import Negative8 from '../assets/negative/negative8.png';
import '../ui/home-card.ui';
import { state } from '../states/state';

export class HomePage extends HTMLElement {
  constructor () {
    super();
  }

  connectedCallback () {
    const results = snapshot(state).totalCO2;

    this.innerHTML = `
      <style>
        genk-home-page > .home-section {
          padding-block: 4rem;
        }
        .home-section header, .home-banner {
          margin-bottom: 3rem;
        }
        genk-home-page > .home-section:nth-child(even) {
          background-color: var(--clr-primary-100);
        }

        .home-banner {
          position: relative;
          display: flex;
          flex-direction: column;
          gap: 2rem;
          padding: 9rem 6rem 0 6rem;
          height: 100vh;
          font-size: clamp(1.3rem, 2.3vw, var(--fs-500));

          background-color: var(--clr-primary-100)
        }
        .home-banner::before {
          content: '';
          position: absolute;
          right: 3rem;
          top: 50%;
          transform: translateY(-50%);
          width: 50%;
          aspect-ratio: 1;
          background-image: url('${BannerImg}');
          background-repeat: no-repeat;
          background-size: cover;
          background-position: right bottom;
        }
        .home-banner > *:not(:last-child) {
          position: relative;
          z-index: 1;
        }

        .home-banner__title, .home-section h2 {
          text-shadow: 1px 1px 2px rgba(6, 95, 142, 0.319);
        }
        .home-banner__title {
          font-size: clamp(2.2rem, 6vw, 3.5rem);
        }

        .home-banner__text {
          line-height: 1.6;
          width: 33vw;
        }
        genk-home-page em {
          color: var(--clr-secondary-300);
          font-weight: 600;
        }

        .home-banner__nav {
          display: flex;
          flex-direction: column;
          align-items: start;
          gap: 2rem;
        }
        .home-banner__nav > a {
          width: 350px;
          text-align: center;
          color: var(--clr-secondary-300);
          font-weight: 600;
          transition: all 0.3s ease-out;
        }
        a.btn-cta {
          font-size: 1.15em;
          padding: 0.5em;
          color: var(--clr-dark-100);
          background-color: var(--clr-secondary-300);
          border-radius: 100vw;
          box-shadow: 0px -4px 20px rgba(0, 0, 0, 0.1);
        }
        a.btn-cta:hover, a.btn-cta:focus-visible {
          color: var(--clr-secondary-300);
          background-color: var(--clr-dark-100);
          box-shadow: 2px 4px 20px rgba(0, 0, 0, 0.2);
        }
        a.btn-cta:focus-visible {
          outline-offset: 5px;
          outline: 2px dashed var(--clr-secondary-300);
        }

        .btn-view-results {
          font-size: 0.85em;
        }

        .card__title {
          font-size: 1.3rem;
          line-height: 1.2;
          color: var(--clr-primary-400)
        }

        .home-section.grid-section {
          display: flex;
          gap: 5rem;
          height: 100vh;
          padding-inline: 5rem;
        }

        .grid-section__content {
          padding-inline: 2rem;
        }

        .home-grid {
          order: -1;
          display: grid;
          gap: 10px;
          grid-template-columns: repeat(12, 40px);
          grid-template-rows: repeat(12, 40px);
        }
        
        .grid-section__content > p {
          font-size: clamp(1.15rem, 1.5vw, 1.3rem);
        }
        .home-grid > * {
          width: 100%;
          border-radius: 50%;
          padding: 1rem;
          box-shadow: rgba(201, 1, 1, 0.346) -10px -20px 60px -12px inset,
            rgba(70, 28, 28, 0.3) -5px -18px 36px -18px inset;
          transition: transform 0.1s ease-in;
        }
        .home-grid > *:hover {
          transform: scale(1.1);
        }
        
        .one {
          grid-column: 5 / 7;
        }
        .two {
          grid-column: 1 / 3;
          grid-row: 5 / 7;
        }
        .three {
          grid-column: 3 / 7;
          grid-row: 3 / 7;
        }
        .four {
          grid-column: 7 / 12;
          grid-row: 2 / 7;
        }
        .five {
          grid-column: 2 / 7;
          grid-row: 7 / 12;
        }
        .six {
          grid-column: 7 / 11;
          grid-row: 7 / 11;
        }
        .seven {
          grid-column: 11 / 13;
          grid-row: 7 / 9;
        }
        .eight {
          grid-column: 7 / 9;
          grid-row: 11 / 13;
        }

        .home-footer {
          text-align: center;
          padding-block: 3rem;
          color: var(--clr-dark-100);
          background-color: var(--clr-dark-500);
        }

        .home-banner__scroll {
          display: flex;
          flex-direction: column;
          align-items: center;
          gap: 0.5em;
          position: absolute;
          left: 0;
          bottom: 0;
          width: 100%;
          padding-block: 1rem;
          color: var(--clr-primary-400);
        
          overflow: hidden;
        }
        .home-banner__scroll p {
          font-size: 0.9rem;
        }
        .home-banner__scroll path {
          fill: var(--clr-primary-400);
        }
        .home-banner__scroll svg {
          animation: iconScroll 1.5s 0s infinite normal cubic-bezier(.645, .045, .355, 1) both;
          opacity: 1;
          transform: translateY(20px);
        }
        
        @keyframes iconScroll {
          0% {
            transform: translateY(0);
          }
          50% {
            transform: translateY(50px);
            opacity: 0;
          }
          51% {
            transform: translateY(-15px);
          }
          100% {
            transform: translateY(0);
            opacity: 1;
          }
        }

        .home-media-scroller {
          display: grid;
          grid-auto-flow: column;
          gap: 3.5rem;
          margin-inline: 2rem;
          padding-block: 1.5rem;

          scroll-snap-type: inline mandatory;

          overflow-x: auto;
          scroll-behavior: smooth;
          overscroll-behavior-inline: contain;
          -ms-overflow-style: none;
          scrollbar-width: none;
        }
        .home-media-scroller::-webkit-scrollbar {
          display: none;
        }
        .home-media-scroller > * {
          scroll-snap-align: start;
        }

        @media only screen and (max-width: 1160px) {
          .home-section.grid-section {
            padding-inline: 1.5rem;
          }
          .grid-section__content {
            padding-inline: 0;
          }
          .home-section.grid-section {
            gap: 2.5rem;
          }
        }
        @media only screen and (max-width: 992px) {
          .home-banner {
            padding: 5rem;
            padding-bottom: 7rem;
          }
          .home-banner__nav {
            margin-top: 2rem;
          }
          .home-section.grid-section {
            flex-direction: column;
          }
          .home-section header, .home-banner {
            margin-bottom: 1.5rem;
          }
          genk-home-page > .home-section {
            padding-block: 2rem;
          }
          .home-grid {
            order: 0;
            grid-template-columns: repeat(12, 20px);
            grid-template-rows: repeat(12, 20px);
            margin-inline: auto;
          }
          .home-grid > * {
            padding: 0.5rem;
          }
        }
        @media only screen and (max-width: 768px) {
          .home-banner::before {
            width: 35%;
            top: 40%;
          }
          .home-banner__text {
            width: 40vw;
          }
          .home-banner__nav {
            align-items: center;
            margin-top: auto;
          }
          .home-grid {
            grid-template-columns: repeat(12, 15px);
            grid-template-rows: repeat(12, 15px);
          }
        }
        @media only screen and (max-width: 570px) {
          .home-banner {
            padding: 4rem 1.3rem 8rem 1.3rem;
          }
          .home-banner::before {
            transform: translateX(-50%);
            top: 48%;
            width: 50%;
            left: 50%
          }
          .home-banner__text {
            width: 100%;
          }
          .home-section.grid-section {
            height: auto;
          }
        }

      </style>

      <header role="banner" class="home-banner">
        <h1 class="home-banner__title">Earth Loves You,</br>Love it Back</h1>
        <p class="home-banner__text">Wanna know your <em>Carbon Footprint</em> per year? Carbon emissions from your lifestyle choices, from traveling to buy new clothes, impact the climate. This is a global problem, but <em>You can be a part of the solution!</em></p>
        <nav aria-label="primary navigation" class="home-banner__nav">
          <a href="/questionnaire" title="QUESTIONNAIRE" class="btn-cta">${(!results && results !== 0) ? 'Take' : 'Retake'} questionnaire</a>
          ${(!results && results !== 0) ? '' : '<a href="/results" title="RESULTS" class="btn-view-results">View Results</a>'}
        </nav>
        <div class="home-banner__scroll">
          <p>SCROLL</p>
          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
            <path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"/>
          </svg>
        <div>
      </header>

      <section id="reason" class="home-section grid-section" aria-label="Environmental problems">
        <div class="grid-section__content">
          <header>
            <h2 id="homeTitle1" class="size-700">Why is it so important?</h2>
          </header>
          <p>The Planet Earth is in crisis - from climate change to the pollution in our oceans and devastation of our forests. <em>It's up to all of us to fix it</em>. Take your first step with our environmental footprint calculator.
            </br>
            </br>
            We're all concerned about climate change, but when it looks like a problem for future generations, you ask yourself, 'will climate change even affect me?' No matter what you care about, climate change is already affecting our world today.
            </br>
            <a href="https://www.wwf.org.uk/updates/10-reasons-why-climate-change-important" target="_blank" class="learn-more">Learn more</a>
          </p>
        </div>
        <div class ="home-grid">
          <img loading="lazy" src="${Negative1}" title="Hole in the ozone layer" alt="Hole in the ozone layer" class="one">
          <img loading="lazy" src="${Negative4}" title="Car fumes" alt="Car fumes" class="two">
          <img loading="lazy" src="${Negative3}" title="High temperatures" alt="High temperatures" class="three">
          <img loading="lazy" src="${Negative7}" title="Melting Earth's poles" alt="Melting Earth's poles" class="four">
          <img loading="lazy" src="${Negative5}" title="Polution"" alt="Polution" class="five">
          <img loading="lazy" src="${Negative6}" title="Floods"" alt="Floods" class="six">
          <img loading="lazy" src="${Negative2}" title="Carbon dioxide emissions" alt="Carbon dioxide emissions" class="seven">
          <img loading="lazy" src="${Negative8}" title="Acid rain" alt="Acid rain" class="eight">
        </div>
      </section>

      <section id="options" class="home-section text-center" aria-label="Actions to reduce carbon dioxide">
        <header class="p-in-2">
          <h2 id="homeTitle2" class="size-700">Okay, what can we do?</h2>
        </header>
        <div class="home-media-scroller">
          <genk-home-card>
            <h3 slot="title" class="card__title">Sustainability</h3>
            <img loading="lazy" slot="image" src="${Positive1}" alt="Sustainable planet earth">
            <p slot="description">Balance between the human being and the resources of his environment.</p>
          </genk-home-card>
          <genk-home-card>
            <h3 slot="title" class="card__title">Reforestation</h3>
            <img loading="lazy" slot="image" src="${Positive2}" alt="Flower in a hand">
            <p slot="description">Planting billions of trees could be a solution to fight climate change</p>
          </genk-home-card>
          <genk-home-card>
            <h3 slot="title" class="card__title">Green companies</h3>
            <img loading="lazy" slot="image" src="${Positive3}" alt="Green companies">
            <p slot="description">A green company claims to act in a way which minimizes damage to the environment.</p>
          </genk-home-card>
          <genk-home-card>
            <h3 slot="title" class="card__title">Clean energies</h3>
            <img loading="lazy" slot="image" src="${Positive4}" alt="Solar panel">
            <p slot="description">Clean energy produce power without having negative environmental impacts.</p>
          </genk-home-card>
          <genk-home-card>
            <h3 slot="title" class="card__title">Eletric cars</h3>
            <img loading="lazy" slot="image" src="${Positive5}" alt="Eletric car">
            <p slot="description">Electric cars are energy efficient and net good for the environment.</p>
          </genk-home-card>
          <genk-home-card>
            <h3 slot="title" class="card__title">Reduce, Reuse, Recycle</h3>
            <img loading="lazy" slot="image" src="${Positive6}" alt="Recycle symbol">
            <p slot="description">It helps the community, and the environment by saving money, energy, and natural resources</p>
          </genk-home-card>
        </div>
      </section>

      <footer class="home-footer">
        <p>Handcrafted with ❤️ by <a href="https://www.linkedin.com/in/pablodevs/" target="_blank" title="AUTHOR">pablodevs</a></p>
      </footer>
    `;
  }
}

customElements.define('genk-home-page', HomePage);
