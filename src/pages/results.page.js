import { snapshot } from 'valtio/vanilla';
import '../components/logo.component';
import { state } from '../states/state';
import '../ui/results.ui';

export class ResultsPage extends HTMLElement {
  constructor () {
    super();
  }

  connectedCallback () {
    const results = snapshot(state).totalCO2;
    if (!results && results !== 0) {
      const redirectEvent = new CustomEvent('vaadin-router-go', {
        bubbles: true,
        composed: true,
        detail: {
          pathname: '/'
        }
      });
      this.dispatchEvent(redirectEvent);
    }

    this.innerHTML = `
      <style>
        genk-results-page {
          display: flex;
          flex-direction: column;
        }

        .results-header > div:first-child {
          display: flex;
          align-items: baseline;
          gap: 2rem;
        }
        .results-header > nav {
          order: -1;
        }

        .results-header__title {
          display: inline;
        }

        .results-section {
          position: relative;
          flex: 1;
          display: flex;
          align-items: stretch;
          flex-wrap: wrap;
          gap: 1.5rem;
          margin: 1.5rem;
          padding: 1.5rem;
          border-radius: 0.5rem;

          box-shadow: 4px 5px 12px rgba(0, 0, 0, 0.1);
          background-color: var(--clr-primary-100);
        }
      </style>
      <header class="results-header page-header">
        <div>
          <h1 class="results-header__title">Results</h1>
          <genk-logo role="logo"></genk-logo>
        </div>  
        <nav aria-label="primary navigation" class="primary-nav">
          <a href="/" title="HOME">
            Go back Home
          </a>
        </nav>
      </header>
      <genk-results-summary class="results-section" aria-label="Questionnaire results"></genk-results-summary>
    `;
  }
}

customElements.define('genk-results-page', ResultsPage);
