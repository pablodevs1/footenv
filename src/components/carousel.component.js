import { css, html, LitElement } from 'lit';
import { SLIDE_ENTER, SLIDE_LEAVE } from '../services/animations.service';

export class CarouselComponent extends LitElement {
  static get styles () {
    return css`
      :host {
        position: relative;
        display: flex;
        align-items: start;
        justify-content: start;
        width: min(500px, 100%);
        height: 100%;
      }

      ::slotted(.slide-active) {
        display: block;
      }
      
      ::slotted(*) {
        position: absolute;
        display: none;
        height: 100%;
        padding: 2em;
        margin-top: 1.5rem;
        opacity: 0;
      }

      .navigation {
        display: flex;
        gap: 2rem;
        padding: 1rem;
      }
    `;
  }

  static get properties () {
    return {
      slideIndex: {
        type: Number,
        state: true
      }
    };
  }

  constructor () {
    super();

    this.slideIndex = 0;

    this._navigateToNextSlide = this._navigateToNextSlide.bind(this);
    this._navigateToPrevSlide = this._navigateToPrevSlide.bind(this);
  }

  connectedCallback () {
    super.connectedCallback();
    document.addEventListener('genk-answer-event', this._navigateToNextSlide);
    document.addEventListener('genk-goto-prev-slide', this._navigateToPrevSlide);
  }

  disconnectedCallback () {
    document.removeEventListener('genk-answer-event', this._navigateToNextSlide);
    document.removeEventListener('genk-goto-prev-slide', this._navigateToPrevSlide);
    super.disconnectedCallback();
  }

  get _slottedChildren () {
    const slot = this.shadowRoot.querySelector('slot');
    return slot.assignedElements({ flatten: true });
  }

  firstUpdated () {
    this._initializeSlides();
  }

  _initializeSlides () {
    for (let i = 0; i < this._slottedChildren.length; i++) {
      if (i === this.slideIndex) {
        this.showSlide(this._slottedChildren[i]);
      } else {
        this.hideSlide(this._slottedChildren[i]);
      }
    }
  }

  hideSlide (element) {
    element.classList.remove('slide-active');
  }

  showSlide (element) {
    element.classList.add('slide-active');
    element.animate(...SLIDE_ENTER);
  }

  _navigateToNextSlide () {
    this._navigateWithAnimation(1);
  }

  _navigateToPrevSlide () {
    this._navigateWithAnimation(-1);
  }

  async _navigateWithAnimation (offset) {
    const numOfSlides = this._slottedChildren.length;
    const nextSlideIndex = this.slideIndex + offset;

    if (nextSlideIndex < 0) {
      return;
    }

    const elLeaving = this._slottedChildren[this.slideIndex];
    const leavingAnimation = elLeaving.animate(...SLIDE_LEAVE);

    if (nextSlideIndex !== numOfSlides) {
      this.slideIndex = nextSlideIndex;
      const newSlideEl = this._slottedChildren[nextSlideIndex];
      this.showSlide(newSlideEl);
    }

    await leavingAnimation.finished;
    this.hideSlide(elLeaving);
  }

  render () {
    return html`<slot></slot>`;
  }
}

customElements.define('genk-carousel', CarouselComponent);
