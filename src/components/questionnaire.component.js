import 'genk-step-progress-bar';
import { html, LitElement } from 'lit';
import { SLIDE_ENTER } from '../services/animations.service';
import QUESTIONS from '../services/questions.service';
import '../styles/components/questionnaire.css';
import { RequestUseCase } from '../usecases/request.usecase';
import './carousel.component';

export class QuestionnaireComponent extends LitElement {
  static get properties () {
    return {
      answers: {
        type: Object,
        state: true
      },
      slideIndex: {
        type: Number,
        state: true
      }
    };
  }

  constructor () {
    super();
    this.answers = new Map();
    this.slideIndex = 0;
  }

  _checkAnswer (event) {
    const answer = event.target.value;

    const checkAnswerEvent = new CustomEvent('genk-answer-event', {
      bubbles: true,
      composed: true,
      detail: { inputValue: answer }
    });
    this.dispatchEvent(checkAnswerEvent);

    this.dispatchEvent(new Event('genk-spb-next', {
      bubbles: true,
      composed: true
    }));

    this.slideIndex++;

    this._saveAnswer(answer);
  }

  _saveAnswer (answer) {
    const requestUseCase = new RequestUseCase();
    const { emission_factor, parameters } = requestUseCase.execute(QUESTIONS, answer);
    this.answers.set(emission_factor.id, { emission_factor: emission_factor, parameters: parameters });

    if (QUESTIONS.length === this.answers.size) {
      setTimeout(() => {
        this._sendRequest();
      }, 800);
    }
  }

  _sendRequest () {
    const submitEvent = new CustomEvent('genk-submit-event', {
      bubbles: true,
      composed: true,
      detail: { request: this.answers }
    });
    this.dispatchEvent(submitEvent);
  }

  _sendPrevSlideEvent () {
    const prevSlideEvent = new Event('genk-goto-prev-slide', {
      bubbles: true,
      composed: true
    });
    this.dispatchEvent(prevSlideEvent);

    this.dispatchEvent(new Event('genk-spb-prev', {
      bubbles: true,
      composed: true
    }));

    if (this.slideIndex > 0) {
      this.slideIndex--;
    }
  }

  firstUpdated () {
    this.querySelector('#prevSlideButton').animate(...SLIDE_ENTER);
    this.querySelector('genk-step-progress-bar').animate(...SLIDE_ENTER);
  }

  render () {
    return html`
      <form id="questionnaire" class="questionnaire">
        <genk-carousel>
          ${QUESTIONS.map((question) => html`
          <fieldset id="${question.id}" class="questionnaire__fieldset">
            <legend class="fieldset__legend size-600">${question.title.en}</legend>
            ${question.answers.map((answer, index) => html`
              <label class="fieldset__answer" for="${`${question.id}-${index}`}">
                <input
                  type="radio"
                  id="${`${question.id}-${index}`}"
                  name="${question.id}"
                  value="${`${question.id}-${index}`}"
                  @click="${this._checkAnswer}">
                <span>${answer.text}</span>
              </label>
            `)}
          </fieldset>
          `)}
        </genk-carousel>
        <nav aria-label="Questionnaire Navigation">
          <button
            id="prevSlideButton"
            class="slide-button"
            type="button" 
            @click="${this._sendPrevSlideEvent}"
          >
            Previous question
          </button>
        </nav>
      </form>
      <genk-step-progress-bar .steps="${QUESTIONS}"></genk-step-progress-bar>
      `;
  }

  createRenderRoot () {
    return this;
  }
}

customElements.define('genk-questionnaire', QuestionnaireComponent);
