import { css, html, LitElement } from 'lit';

export class LogoComponent extends LitElement {
  static get styles () {
    return css`
      :host {
        --logo-size: 2.5rem;
        --logo-clr: var(--clr-primary-500)
      }

      *, *::before, *:after {
        box-sizing: border-box;
      }

      .logo__building {
        position: relative;
        width: var(--logo-size);
        aspect-ratio: 2 / 1;
        border-radius: calc(var(--logo-size) / 33);
        border-start-start-radius: 0;
        background-color: var(--logo-clr);
        color: var(--logo-clr);
      }
      .logo__building::before,
      .logo__building::after {
        content: '';
        position: absolute;
        top: 0;
        left: 0;
        transform: translateY(-100%);
        border-color: inherit;
        border-bottom: calc(var(--logo-size) / 5.5) solid;
        border-left: calc(var(--logo-size) / 3.47) solid transparent;
      }
      .logo__building::after {
        transform: translate(100%, -100%);
      }

      .building__window {
        position: absolute;
        bottom: calc(var(--logo-size) / 8.25);
        left: calc(var(--logo-size) / 8.25);
        width: calc(var(--logo-size) / 8.25);
        height: calc(var(--logo-size) / 6.6);
        background-color: white;
      }
      .building__window::before {
        content: '';
        position: absolute;
        transform: translateY(-50%);
        width: 100%;
        aspect-ratio: 1 / 1;
        border-radius: 50%;
        background-color: inherit;
      }
      .building__window:nth-child(2) {
        left: calc(var(--logo-size) / 2.87);
      }

      .building__chimney {
        position: absolute;
        bottom: 100%;
        right: calc(var(--logo-size) / 22);
        width: calc(var(--logo-size) / 3);
        height: calc(var(--logo-size) / 2.2);

        border-color: inherit;
        border-left: calc(var(--logo-size) / 13.2) solid transparent;
        border-right: calc(var(--logo-size) / 13.2) solid transparent;
        border-bottom: calc(var(--logo-size) / 1.65) solid;
      }

      .building__smokes {
        position: absolute;
        top: calc(-1 * calc(var(--logo-size) / 1.65));
        right: calc(var(--logo-size) / 4.89);
        transform: translate(-50%, -50%);
      }

      .building__smokes > div {
        position: absolute;
        top: calc(-1 * calc(var(--logo-size) / 16.5));
        left: calc(-1 * calc(var(--logo-size) / 16.5));
        aspect-ratio: 1 / 1;
        border-radius: 50%;
        background-color: var(--logo-clr);
      }

      .building__smokes > div:nth-child(even) {
        animation: smokeEven 2.8s linear infinite;
      }

      .building__smokes > div:nth-child(odd) {
        animation: smokeOdd 2.8s linear infinite;
      }

      @keyframes smokeEven {
        0% {
          width: calc(var(--logo-size) / 8.25);
          transform: translate(0, 0) scale(1);
          filter: blur(calc(var(--logo-size) / 33));
          opacity: 1;
        }

        100% {
          width: calc(var(--logo-size) / 4.4);
          transform: translate(calc(var(--logo-size) / 3.3), calc(-1 * calc(var(--logo-size) / 0.66))) scale(3);
          filter: blur(calc(var(--logo-size) / 13.2));
          opacity: 0;
        }
      }

      @keyframes smokeOdd {
        0% {
          width: calc(var(--logo-size) / 8.25);
          transform: translate(0, 0) scale(1);
          filter: blur(calc(var(--logo-size) / 33));
          opacity: 1;
        }

        100% {
          width: calc(var(--logo-size) / 4.4);
          transform: translate(calc(-1 * calc(var(--logo-size) / 3.3)), calc(-1 * calc(var(--logo-size) / 0.66))) scale(3);
          filter: blur(calc(var(--logo-size) / 13.2));
          opacity: 0;
        }
      }

      .building__smokes > div:nth-child(1) {
        animation-delay: 0s
      }

      .building__smokes > div:nth-child(2) {
        animation-delay: 0.4s
      }

      .building__smokes > div:nth-child(3) {
        animation-delay: 0.8s
      }

      .building__smokes > div:nth-child(4) {
        animation-delay: 1.2s
      }

      .building__smokes > div:nth-child(5) {
        animation-delay: 1.6s
      }

      .building__smokes > div:nth-child(6) {
        animation-delay: 2s
      }

      .building__smokes > div:nth-child(7) {
        animation-delay: 2.4s
      }

      .building__smokes > div:nth-child(8) {
        animation-delay: 2.8s
      }
    `;
  }

  render () {
    return html`
      <div class="logo__building">
        <div class="building__window"></div>
        <div class="building__window"></div>
        <div class="building__chimney"></div>
        <div class="building__smokes">
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
    `;
  }
}

customElements.define('genk-logo', LogoComponent);
