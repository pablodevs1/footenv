import { css, html, LitElement } from 'lit';

export class ChartComponent extends LitElement {
  static get styles () {
    return [
      css`
        *, *::before, *::after {
          box-sizing: border-box;
        }
      `,
      css`
        :host {
          position: relative;
          width: 158px;
          aspect-ratio: 1;
        }
        .outer {
          display: grid;
          place-items: center;
          background-color: white;
          box-shadow: 6px 6px 10px -1px rgba(0,0,0,0.15),
                      -6px -6px 10px -1px rgba(255,255,255,0.7)
        }
        .outer, .inner {
          width: 100%;
          aspect-ratio: 1;
          border-radius: 50%;
        }
        .inner {
          display: grid;
          place-items: center;
          width: 80%;
          background-color: var(--charts--bg-color);
          box-shadow: inset 4px 4px 6px -1px rgba(0, 0, 0, 0.2),
                      inset -4px -4px 6px -1px rgba(255, 255, 255, 0.7),
                      -0.5px -0.5px 0px rgba(255, 255, 255, 1),
                      0.5px 0.5px 0px rgba(0, 0, 0, 0.15),
                      0px 12px 10px -10px rgba(0, 0, 0, 0.05)
        }

        .badge {
          font-size: 1.5em;
        }
        .badge, .label {
          font-weight: 600;
          text-shadow: 0 0 1px rgba(0, 0, 0, 0.20);  
        }
        .label {
          text-align: center;
          font-size: 1.3em;
          margin: 0;
          margin-top: 0.75em;
        }

        circle {
          fill: none;
          stroke-width: 16px;
          stroke-dasharray: 440;
          stroke-dashoffset: 440;
        }
        svg {
          position: absolute;
          top: 0;
          left: 0;
        }
      `
    ];
  }

  static get properties () {
    return {
      percentage: { type: Number },
      color: { type: String },
      value: { type: Number },
      category: { type: String },
      counter: { type: Number, state: true }
    };
  }

  constructor () {
    super();
    this.counter = 0;
  }

  firstUpdated () {
    const CHART_ANIM_KEYFRAMES = [
      { strokeDashoffset: 440 - (440 * this.percentage / 100) }
    ];
    const CHART_ANIM_OPTS = {
      duration: 1000,
      easing: 'linear',
      fill: 'forwards'
    };
    const CHART_ANIM = [
      CHART_ANIM_KEYFRAMES,
      CHART_ANIM_OPTS
    ];

    setInterval(() => {
      if (this.counter === this.percentage) {
        clearInterval();
      } else {
        this.counter += 1;
      }
    }, 1000 / this.percentage);
    this.shadowRoot.querySelector('circle').animate(...CHART_ANIM);
  }

  render () {
    return html`
      <div class="outer">
        <div class="inner">
          <p class="badge" style="color: ${this.color}">${this.counter}%</p>
        </div>
      </div>
      <svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="160px" height="160px" role="img" aria-labelledby="${this.category}Title ${this.category}Desc">
        <title id="${this.category}Title">Pie chart for ${this.category} contribution</title>
        <desc id="${this.category}Desc">An animated pie chart describing the contribution to carbon footprint for ${this.category} category</desc>
        <circle cx="80" cy="80" r="70" stroke-linecap="round" style="stroke: ${this.color};"/>
      </svg>
      <p class="label" style="color: ${this.color}">${this.category}</p>
    `;
  }
}

customElements.define('genk-chart', ChartComponent);
