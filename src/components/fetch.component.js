import { css, html, LitElement } from 'lit';
import { snapshot } from 'valtio/vanilla';
import { state } from '../states/state';
import { BatchFetchUseCase } from '../usecases/batch-fetch.usecase';
import { Co2UseCase } from '../usecases/co2.usecase';
import { ChartsUseCase } from '../usecases/charts.usecase';

export class FetchComponent extends LitElement {
  static get styles () {
    return css`
      :host {
        display: flex;
        justify-content: center;
        width: 60%;
        margin-top: 1rem;
        background-color: var(--clr-primary-100);
        box-shadow: 0px -4px 20px rgba(0, 0, 0, 0.1);
      }
    `;
  }

  constructor () {
    super();

    this._fetchAPI = this._fetchAPI.bind(this);
  }

  connectedCallback () {
    super.connectedCallback();
    document.addEventListener('genk-submit-event', this._fetchAPI);
  }

  disconnectedCallback () {
    document.removeEventListener('genk-submit-event', this._fetchAPI);
    super.disconnectedCallback();
  }

  async _fetchAPI (event) {
    const requestValues = [...event.detail.request.values()];
    const batchUseCase = new BatchFetchUseCase();
    const response = await batchUseCase.execute(requestValues);

    const co2UseCase = new Co2UseCase();
    state.totalCO2 = co2UseCase.execute(response);

    const chartsUseCase = new ChartsUseCase();
    state.charts = chartsUseCase.execute(response, snapshot(state).totalCO2);

    this._redirectTo('results');
  }

  _redirectTo (page) {
    const redirectEvent = new CustomEvent('vaadin-router-go', {
      bubbles: true,
      composed: true,
      detail: {
        pathname: `/${page.toLowerCase()}`
      }
    });
    this.dispatchEvent(redirectEvent);
  }

  render () {
    return html`<slot></slot>`;
  }
}

customElements.define('genk-fetch', FetchComponent);
