import { UtilsService } from '../../src/services/utils.service';

describe('Trunc number static class method', () => {
  it('Trunc number correctly', () => {
    const num1 = 123.689;
    const num2 = 0.00002;
    const num3 = -123.4545;
    const num4 = 999.1145;
    const num5 = 1.3333333;
    const num6 = 0.555555556;
    const num7 = 99.99999;
    const num8 = 0;
    const num9 = -0;

    const num1Truncated = UtilsService.trunc(num1);
    const num2Truncated = UtilsService.trunc(num2);
    const num3Truncated = UtilsService.trunc(num3);
    const num4Truncated = UtilsService.trunc(num4);
    const num5Truncated = UtilsService.trunc(num5);
    const num6Truncated = UtilsService.trunc(num6);
    const num7Truncated = UtilsService.trunc(num7);
    const num8Truncated = UtilsService.trunc(num8);
    const num9Truncated = UtilsService.trunc(num9);

    expect(num1Truncated).toBe(123.69);
    expect(num2Truncated).toBe(0);
    expect(num3Truncated).toBe(-123.45);
    expect(num4Truncated).toBe(999.11);
    expect(num5Truncated).toBe(1.33);
    expect(num6Truncated).toBe(0.56);
    expect(num7Truncated).toBe(100);
    expect(num8Truncated).toBe(0);
    expect(num9Truncated).toBe(0);
  });
});
