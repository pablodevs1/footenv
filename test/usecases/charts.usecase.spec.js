import BATCH from '../../fixtures/batch.json';
import { ChartsUseCase } from '../../src/usecases/charts.usecase';

const TOTAL_CO2_IN_TONNES = 5.19;

describe('Get Results Charts Objects Use Case', () => {
  it('should execute correct', () => {
    const useCase = new ChartsUseCase();
    const charts = useCase.execute(BATCH.results, TOTAL_CO2_IN_TONNES);

    expect(charts).toHaveLength(3);
    expect(Object.keys(charts[0])).toHaveLength(3);
    expect(Object.keys(charts[1])).toHaveLength(3);
    expect(Object.keys(charts[2])).toHaveLength(3);
    expect(charts[0].color).toBe('#ff0000'); // red
    expect(charts[1].color).toBe('#ffaa00'); // orange
    expect(charts[2].color).toBe('#00d900'); // green
    expect(charts[0].percentage).toBe(61);
    expect(charts[1].percentage).toBe(24);
    expect(charts[2].percentage).toBe(14);
  });
});
