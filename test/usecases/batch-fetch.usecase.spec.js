import BATCH from '../../fixtures/batch.json';
import REQUEST from '../../fixtures/request.json';
import { BatchFetchRepository } from '../../src/repositories/batch-fetch.repository';
import { BatchFetchUseCase } from '../../src/usecases/batch-fetch.usecase';

jest.mock('../../src/repositories/batch-fetch.repository');
describe('Fetch Data Use Case', () => {
  beforeEach(() => {
    BatchFetchRepository.mockClear();
  });

  it('should execute correct', async () => {
    BatchFetchRepository.mockImplementation(() => {
      // Método dentro del repository, se tiene que llamar exactamente igual
      // Devuelve BATCH json en lugar de la llamada:
      return {
        batchFetch: () => {
          return BATCH;
        }
      };
    });

    const useCase = new BatchFetchUseCase();
    const response = await useCase.execute(REQUEST);

    expect(response).toHaveLength(3);
    expect(response[0].hasOwnProperty('co2e')).toBeTruthy();
    expect(response[0].hasOwnProperty('emission_factor')).toBeTruthy();
    expect(response[0].emission_factor.hasOwnProperty('category')).toBeTruthy();
    expect(response[0].co2e).toBe(3196.8985176738884);
    expect(response[1].co2e).toBe(748.0);
    expect(response[2].co2e).toBe(1248.576);
  });
});
