import BATCH from '../../fixtures/batch.json';
import { Co2UseCase } from '../../src/usecases/co2.usecase';

describe('Get CO2 Use Case', () => {
  it('should execute correct', () => {
    const useCase = new Co2UseCase();
    const totalCO2inTonnes = useCase.execute(BATCH.results);

    expect(totalCO2inTonnes).toBe(5.19);
  });
});
