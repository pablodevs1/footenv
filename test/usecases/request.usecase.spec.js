import QUESTIONS from '../../src/services/questions.service';
import { RequestUseCase } from '../../src/usecases/request.usecase';

describe('Get Request Object Use Case', () => {
  it('should execute correct', () => {
    const useCase = new RequestUseCase();
    const { emission_factor: emission_factor1, parameters: parameters1 } = useCase.execute(QUESTIONS, 'question_1-2');
    const { emission_factor: emission_factor2, parameters: parameters2 } = useCase.execute(QUESTIONS, 'question_2-3');

    expect(Object.keys(emission_factor1)).toHaveLength(2);
    expect(Object.keys(emission_factor2)).toHaveLength(2);
    expect(Object.keys(parameters1)).toHaveLength(2);

    // 1
    expect(emission_factor1.id).toBe('consumer_goods-type_clothing');
    expect(emission_factor1.region).toBe('US');
    expect(parameters1.money).toBe(420);
    expect(parameters1.money_unit).toBe('eur');

    // 2
    expect(emission_factor2.id).toBe('accommodation_type_hotel_stay');
    expect(emission_factor2.region).toBe('ES');
    expect(parameters2.number).toBe(11);
  });
});
