<div align="center">
<h1 align="center">FootEnv</h1>
  <p align="center">
    Web app to estimate your environmental footprint.
  </p>
</div>

<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>

## About The Project

FootEnv helps you estimate your environmental footprint created by a broad range of emission-generating activities.
    
Operational activity data can be generated from any actions that result in greenhouse gas emissions, including vehicle rides, energy consumption, flights, logistical operations, manufacturing, agriculture, heating or cooling of buildings, and countless other daily activities.

### Built With

* [Node.js](https://nodejs.org/en/) (v18.0.0)
* [climatiq](https://www.climatiq.io/docs/)
* [Lit](https://lit.dev/)
* [Vaadin router](https://vaadin.com/router)

## Getting Started

It can't be more simple to install the project.

### Installation

1. Get an API Key at [climatiq](https://www.climatiq.io/docs/)
2. Clone the repo
   ```sh
   git clone https://gitlab.com/pablodevs1/footenv.git
   ```
3. Install NPM packages
   ```sh
   npm install
   ```
4. Creates the dotenv file using the .env.example file.
   ```
   API_KEY = ENTER YOUR API KEY HERE;
   ```

## Usage

To run the app you just need to use the following command:
```sh
npm run start
```
## Status

[![Netlify Status](https://api.netlify.com/api/v1/badges/5f77411a-824b-48f7-871c-19a334f81e83/deploy-status)](https://app.netlify.com/sites/footenv/deploys)

## Contact

Pablo Álamo - [LinkedIn](https://www.linkedin.com/in/pablodevs/) - pabloalamovargas@gmail.com

Project Link: [https://gitlab.com/pablodevs1/footenv](https://gitlab.com/pablodevs1/footenv)

<p align="right">(<a href="#top">back to top</a>)</p>
